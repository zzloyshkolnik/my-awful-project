import React from 'react'
import Article from './Article'

export default function ArticleList({article}) {
    const list = article.map(article => 
        <Article article={article} key={article.title}/> /* Конечно key не уникальный, но думаю и так сойдет */
    )
    return (
        <div className="articles">
            {list}
        </div>
    )
}