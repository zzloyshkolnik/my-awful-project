import React, {Component} from 'react'

class Article extends Component {
    state = {
        isShow: true
    }

    handleDeleteImg = () => {
        this.setState({ isShow: false })
    }
    render() {      
        const {article} = this.props
        const img = this.state.isShow && <img className="article-image" src={article.src} alt={article.title}></img>
        return (
            <article>
                <div className="article-header">
                    <h3>{article.title}</h3>
                    <button onClick={this.handleDeleteImg} type="button" className="article-header-delete">Delete</button>
                </div>
                {img}
            </article>
        )
    }
}

export default Article