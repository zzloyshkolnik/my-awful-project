import React, {Component} from 'react'
import Article from './Article'

class Popup extends Component {
    state = {
        isOpen: false,
        inputTitle: '',
        inputUrl: ''
    }

    inputValueTitle = (evt) => {
        this.setState({
            inputTitle: evt.target.value,
        });
        let title = this.state.inputTitle
        console.log("Title: " + title)
    }
    
    inputValueUrl = (evt) => {
        this.setState({
            inputUrl: evt.target.value
        });
        let url = this.state.inputUrl
        console.log("URL: " + url)
    }

    handleClose = () => {
        this.props.updateData(this.state.isOpen)
    }
    
    handleAdd = () => {
        this.props.updateData(this.state.isOpen)
    }
    
    render() {
        return (
            <div className="popup">
                <form className="popup-form" action="">
                    <h3>New Image</h3>
                    <input value={this.state.inputTitle} onChange={evt => this.inputValueTitle(evt)} type="text" name="title" className="popup-title" placeholder="Title"></input>
                    <input value={this.state.inputUrl} onChange={evt => this.inputValueUrl(evt)} type="text" name="url" className="popup-url" placeholder="URL"></input>
                    <div className="btn">
                        <button onClick={this.handleClose} type="button" className="popup-close">Close</button>
                        <button onClick={this.handleAdd} type="button" name="submit" className="popup-submit">Add</button>
                    </div>
                </form>
            </div>
        )
    }
}
/*
function NewPost() {
    const img = this.state.isShow && <img className="article-image" src={this.state.inputUrl} alt={this.state.inputTitle}></img>
        return (
            <article>
                <div className="article-header">
                    <h3>{this.state.inputTitle}</h3>
                    <button onClick={this.handleDeleteImg} type="button" className="article-header-delete">Delete</button>
                </div>
                {img}
            </article>
        )
}
*/

export default Popup