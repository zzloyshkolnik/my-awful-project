import React, {Component} from 'react'

class ButtonAddArticle extends Component {
    state = {
        isOpen: true
    }

    handleClick = () => {
        this.props.updateData(this.state.isOpen)
    }
    render() {
        return (
            <button onClick={this.handleClick} type="button" className="add-article">new</button>
        )
    }
}

export default ButtonAddArticle