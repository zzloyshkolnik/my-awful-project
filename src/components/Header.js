import React from 'react'

function Header() {
    return (
        <header>
            <div className='header-image'></div>
            <h1 className='header-logo'>Images</h1>
        </header>
    )
}

export default Header