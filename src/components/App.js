import React, {Component} from 'react'
import Header from './Header'
import ButtonAddArticle from './ButtonAddArticle'
import ArticleList from './ArticleList'
import Popup from './Popup'
import articles from '../articles'
import '../style.css'
import '../media.css'

class App extends Component {
    state = {
        isOpen: false
    }

    updateData = (value) => {
        this.setState({ isOpen: value })
    }

    render() {      
        const popup = this.state.isOpen && <Popup updateData={this.updateData}/>
        return (
            <div className='wrapper'>
                <Header />
                <section className="main">
                    <ButtonAddArticle updateData={this.updateData}/>
                    <ArticleList article={articles}/>
                </section>
                {popup}
            </div>
        )
    }
}


export default App